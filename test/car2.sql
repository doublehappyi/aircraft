CREATE DATABASE IF NOT EXISTS flash DEFAULT CHARSET utf8;
use flash;
#超级管理员只能有1个，这个人是admin！！！后续如果需要添加普通管理员，则再说！


#营销人员表
CREATE TABLE IF NOT EXISTS manager(
    id INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    username VARCHAR(10) UNIQUE NOT NULL,
    password VARCHAR(20) DEFAULT '88888888',
    realname VARCHAR(10) NOT NULL,
    phonenum VARCHAR(11) UNIQUE NOT NULL,
    status   TINYINT(1)   DEFAULT 1, #0禁用， 1启用
    datetime INT(10) NOT NULL
);
ALTER TABLE manager auto_increment=1000000000;

#客户表
CREATE TABLE IF NOT EXISTS client(
    id INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    realname VARCHAR(20) DEFAULT NULL,
    gender   TINYINT(1) DEFAULT 1, #1男，0女
    birthday VARCHAR(10) DEFAULT NULL, #YYYY-MM-DD
    phonenum VARCHAR(11) UNIQUE DEFAULT NULL,
    interest CHAR(1)   DEFAULT 2, #A,B,C,D
    manager_id INT(10) NOT NULL,
    result   TINYINT(1) NOT NULL, #0战败结束，1成交,2保有，
    datetime INT(10) NOT NULL
);